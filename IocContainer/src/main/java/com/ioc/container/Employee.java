package com.ioc.container;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Employee {
    public static final Logger logger=LogManager.getLogger(Employee.class);
    private int employeeId;
    private String employeeName;
    private long employeeMobileNumber;
    private Salary salary;
    private List<String> teamMembers;

    Employee(int id,String name,long mob,Salary salary,List<String> teamMembers) //Dependency injection by constructor (Dependent object)
    {
        this.employeeId=id;
        this.employeeName=name;
        this.employeeMobileNumber=mob;
        this.salary=salary;
        this.teamMembers=teamMembers;
    }

    public void getInfo()
    {
        logger.info("Employee id : {}",employeeId);
        logger.info("Employee name : {}",employeeName);
        logger.info("Employee mobile number : {}",employeeMobileNumber);
        salary.displaySalary();
        logger.info("Team Members are :");
        for (String teamMember:teamMembers) {           //Constructor injection with collection
            logger.info("{}",teamMember);
        }
    }
}
