package com.ioc.container;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.beans.factory.xml.*;
import org.springframework.beans.factory.*;
import org.springframework.core.io.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class MainApp {
    public static final Logger logger=LogManager.getLogger(MainApp.class);

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");//Using Application context
        IocDemo iocDemoApplicationContext = (IocDemo) context.getBean("iocDemo");
        iocDemoApplicationContext.setMessage("Inversion of container using Application Context");
        iocDemoApplicationContext.getMessage();

        Resource resource=new ClassPathResource("Beans.xml");   //Using bean factory
        BeanFactory factory=new XmlBeanFactory(resource);
        IocDemo iocDemoBeanFactory=(IocDemo) factory.getBean("iocDemo");
        iocDemoBeanFactory.setMessage("Inversion of container using Bean Factory");
        iocDemoBeanFactory.getMessage();


        ApplicationContext ctxt = new ClassPathXmlApplicationContext("EmployeeConfig.xml","Beans.xml");
        Employee employee=(Employee) ctxt.getBean("employee");      //Dependency Injection using Constructor
        logger.info("*********Employee Details**********");
        employee.getInfo();

    }
}
