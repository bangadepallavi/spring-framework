package com.ioc.container;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class Salary {
    public static final Logger logger=LogManager.getLogger(Salary.class);
    private long basicSalary;
    private  long grossSalary;
    private long netSalary;

    Salary()
    {
        logger.info("Salary details");
    }
    Salary(long basicSalary,long grossSalary,long netSalary)
     {
         this.basicSalary=basicSalary;
         this.grossSalary=grossSalary;
         this.netSalary=netSalary;
     }
     public void displaySalary()
     {
         logger.info("Basic salary : {}",basicSalary );
         logger.info("Gross salary : {}",grossSalary);
         logger.info("Net salary : {}",netSalary);
     }

}
