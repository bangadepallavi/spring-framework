package com.ioc.container;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class IocDemo {
    public static final Logger logger=LogManager.getLogger(IocDemo.class);
//Demo for IOC container with ApplicationContext and BeanFactory
    private String message;
    public void setMessage(String message)
    {
        this.message  = message;
    }
    public void getMessage()
    {
        logger.info("Your Message :{} ",message);
    }
}
