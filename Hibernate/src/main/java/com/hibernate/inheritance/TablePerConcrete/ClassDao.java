package com.hibernate.inheritance.TablePerConcrete;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.Scanner;

public class ClassDao {
    public static final Logger logger = LogManager.getLogger(ClassDao.class);

    public static void main(String[] args) {
        Configuration cfg = new Configuration();
        cfg.configure("ConcreteClass.cfg.xml");
        Transaction transaction=null;
       try(SessionFactory factory = cfg.buildSessionFactory();
             Session session = factory.openSession();Scanner scanner = new Scanner(System.in);)
       {
           logger.info("Please enter your choice");
           logger.info("1)Class details");
           logger.info("2)Professor details");
           logger.info("3)User details");
           int choice=scanner.nextInt();
           ClassDetails classDetails = new ClassDetails();
           Professor professor = new Professor();
           Student student =new Student();
           switch (choice)
           {
             case 1:logger.info("Enter the class code :");
                int classCode = scanner.nextInt();
                 classDetails.setClassCode(classCode);
                logger.info("Enter the professor Id :");
                int professorId = scanner.nextInt();
                 classDetails.setProfessorId(professorId);
                logger.info("Enter the User roo no. :");
                int studentRollNo = scanner.nextInt();
                 classDetails.setStudentRollNo(studentRollNo);
                 transaction = session.beginTransaction();
                 session.save(classDetails);
                break;

             case 2:logger.info("Enter the Professor Id :");
                int id = scanner.nextInt();
                professor.setProfessorId(id);
                logger.info("Enter the professor name :");
                String name = scanner.next();
                professor.setName(name);
                logger.info("Enter the subject assigned to professor :");
                String subject = scanner.next();
                professor.setSubject(subject);
                logger.info("Enter the class code assigned to professor");
                int classCodeOfProf = scanner.nextInt();
                professor.setClassCode(classCodeOfProf);
                 transaction = session.beginTransaction();
                 session.save(professor);
                break;
             case 3:     logger.info("Enter the User roll no  :");
                int studentRoll = scanner.nextInt();
                student.setStudentRollNo(studentRoll);
                logger.info("Enter the User name :");
                String studentName = scanner.next();
                student.setStudentName(studentName);
                logger.info("Enter the User age :");
                int age = scanner.nextInt();
                student.setAge(age);
                logger.info("Enter the class code assigned to User");
                int classCodeOfStudent = scanner.nextInt();
                student.setClassCode(classCodeOfStudent);
                 transaction = session.beginTransaction();
                 session.save(student);
                break;
             default:logger.error("Please enter the proper choice");
           }
           transaction.commit();
       }
       catch (Exception e)
       {
           transaction.rollback();
           logger.error(e);
       }
        logger.info("Thank you");




    }
}
