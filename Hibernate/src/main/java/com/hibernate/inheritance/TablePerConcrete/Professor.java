package com.hibernate.inheritance.TablePerConcrete;

public class Professor extends ClassDetails {
    String name,subject;

    public Professor(){}
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

}
