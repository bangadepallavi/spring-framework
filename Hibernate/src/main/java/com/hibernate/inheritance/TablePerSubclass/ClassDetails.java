package com.hibernate.inheritance.TablePerSubclass;

public class ClassDetails {
    int classCode;
    int professorId,studentRollNo;
    public ClassDetails(){}

    public int getClassCode() {
        return classCode;
    }

    public void setClassCode(int classCode) {
        this.classCode = classCode;
    }

    public int getProfessorId() {
        return professorId;
    }

    public int getStudentRollNo() {
        return studentRollNo;
    }

    public void setProfessorId(int professorId) {
        this.professorId = professorId;
    }

    public void setStudentRollNo(int studentRollNo) {
        this.studentRollNo = studentRollNo;
    }
}
