package com.hibernate.inheritance.TablePerSubclass;

public class Student extends ClassDetails {
    int age;
    String studentName;
public Student(){}

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

}
