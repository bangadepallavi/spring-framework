package com.hibernate.inheritance.TablePerHierarchy;

public class RegularEmployee extends Employee{
    float salary;
    int bonus;

    public RegularEmployee() {}
    public float getSalary() {
        return salary;
    }

    public int getBonus() {
        return bonus;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }
}
