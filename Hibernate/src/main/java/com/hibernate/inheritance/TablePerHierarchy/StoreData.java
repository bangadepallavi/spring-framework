package com.hibernate.inheritance.TablePerHierarchy;
import org.hibernate.Transaction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StoreData {
    public static final Logger logger =LogManager.getLogger(StoreData.class);

    public static void main(String[] args) {
        Configuration cfg = new Configuration();
        cfg.configure("TablePerHierarchy.cfg.xml");
        Transaction transaction=null;
        try (SessionFactory factory = cfg.buildSessionFactory();
             Session session = factory.openSession()) {
            Employee employee = new Employee();
            employee.setName("Nitin");

            RegularEmployee regEmp = new RegularEmployee();
            regEmp.setBonus(1000);
            regEmp.setSalary((float)200);
            ContractEmployee contEmp = new ContractEmployee();
            contEmp.setPayPerHour(300);
            contEmp.setContractPeriod("3 Months");

            transaction = session.beginTransaction();
            session.persist(employee);
            session.persist(regEmp);
            session.persist(contEmp);
            transaction.commit();
            logger.info("successful");
        }
        catch (Exception e)
        {
            transaction.rollback();
            logger.error(e);
        }
    }
}
