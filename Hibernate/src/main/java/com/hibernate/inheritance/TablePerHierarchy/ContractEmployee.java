package com.hibernate.inheritance.TablePerHierarchy;

public class ContractEmployee extends Employee{
    float payPerHour;
    String contractPeriod;

    public ContractEmployee()
    {

    }
    public float getPayPerHour() {
        return payPerHour;
    }

    public String getContractPeriod() {
        return contractPeriod;
    }

    public void setPayPerHour(float payPerHour) {
        this.payPerHour = payPerHour;
    }

    public void setContractPeriod(String contractPeriod) {
        this.contractPeriod = contractPeriod;
    }
}
