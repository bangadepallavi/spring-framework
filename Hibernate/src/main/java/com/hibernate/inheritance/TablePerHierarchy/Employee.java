package com.hibernate.inheritance.TablePerHierarchy;

public class Employee {
    int empid;
    String name;

    public Employee(){}

    public int getEmpid() {
        return empid;
    }

    public String getName() {
        return name;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public void setName(String name) {
        this.name = name;
    }
}
