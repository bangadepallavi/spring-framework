package org.secondLevelCache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
public class UserImpl {
    public static final Logger logger=LogManager.getLogger(UserImpl.class);
    public static void main(String[] args)
    {
        Configuration cfg=new Configuration();
        cfg.configure("SecondLevelCache.cfg.xml");
        try(SessionFactory factory=cfg.buildSessionFactory();
         Session session1 = factory.openSession()) {
            Query query = session1.createQuery("from User ");
            query.setCacheable(true);
            List<User> list = (List<User>) query.list();
            for (User user : list) {
                logger.info("Id : {}", user.getId());
            }
            session1.close();
            logger.info("*****Second level cache ********");
            Session session2 = factory.openSession();
            Query query2 = session2.createQuery("from User ");
            query2.setCacheable(true);
            List<User> list1 = (List<User>) query2.list();
            for (User user : list1) {
                logger.info("Id : {}", user.getId());
            }
        }
        catch (Exception e)
        {
            logger.error("{}",e.getMessage());
        }
    }
}
