package org.hql;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.sql.Update;

import java.util.List;

public class UserImpl {
    public static final Logger logger=LogManager.getLogger(UserImpl.class);
    public static void main(String[] args)
    {
        Configuration cfg=new Configuration();
        cfg.configure("hql.cfg.xml");
        Transaction transaction=null;
            try (SessionFactory factory=cfg.buildSessionFactory();
            Session session = factory.openSession())
            {
            Query query = session.createQuery("from User where age>=:age1 and age<:age2");
              query.setParameter("age1",24);
              query.setParameter("age2",30);
            List<User> empList = query.list();
            for(User emp : empList)
                logger.info("List of Employees:{}:{}",emp.getId(),emp.getName());

            logger.info("*********Update record using HQL**********");
            Query query1=session.createQuery("update User set name= :n where id= :i ");
            query1.setParameter("n","Adwik");
            query1.setParameter("i",103);
            if(query1.executeUpdate()==0)
                logger.error("Can not update");
            else
                logger.info("Successfully update");
            transaction=session.beginTransaction();
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            logger.error(e);
        }
    }
}
