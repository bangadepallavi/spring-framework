package org.hcql;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
//import sun.security.krb5.internal.ccache.Tag;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class UserImpl {
    public static final Logger logger=LogManager.getLogger(UserImpl.class);
    public static void main(String[] args)
    {
        Configuration cfg=new Configuration();
        cfg.configure("hcql.cfg.xml");
        try(SessionFactory factory=cfg.buildSessionFactory();
           Session session = factory.openSession();
           Scanner scanner=new Scanner(System.in)){
            CriteriaBuilder criteriaBuilder=session.getCriteriaBuilder();
            CriteriaQuery<User> criteriaQuery=criteriaBuilder.createQuery(User.class);
            Root<User> root=criteriaQuery.from(User.class);
            logger.info("Enter your choice");
            logger.info("1)sort the users in ascending order their age");
            logger.info("2)Get the users in appropriate age limit");
            logger.info("3)Display the number of users ");
            switch (scanner.nextInt()) {
                case 1:logger.info("sort the users in ascending order their age");
                    criteriaQuery.orderBy(criteriaBuilder.asc(root.get("age")));                //Order
                    List<User> userList=session.createQuery(criteriaQuery).getResultList();
                    for (User user:userList)
                        logger.info("{} : {}",user.getName(),user.getAge());
                    break;
                case 2:logger.info("Enter the lower value");
                    int lowerLimit=scanner.nextInt();
                    logger.info("Enter the higher limit :");
                    int higherLimit=scanner.nextInt();
                    criteriaQuery.where(criteriaBuilder.between(root.get("age"),lowerLimit,higherLimit));               //Restrictions
                    List<User> list=session.createQuery(criteriaQuery.orderBy(criteriaBuilder.asc(root.get("age")))).getResultList();
                    for (User user:list)
                        logger.info("{} : {}",user.getName(),user.getAge());
                    break;
                case  3:logger.info("Number of users are  :");
                    CriteriaBuilder cb = session.getCriteriaBuilder();
                    CriteriaQuery<Long> cq = cb.createQuery(Long.class);
                    cq.select(cb.count(cq.from(User.class)));
                    int count=session.createQuery(cq).getSingleResult().intValue();
                    if (count!=0)
                        logger.info("Total Results: {}", count);
                    else
                        logger.error("No data found ");
                    break;
                default:logger.info("Enter the proper choice . ");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }

    }
}
