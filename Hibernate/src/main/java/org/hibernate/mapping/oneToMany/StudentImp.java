package org.hibernate.mapping.oneToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.List;

public class StudentImp {
    public static final Logger logger=LogManager.getLogger(StudentImp.class);
    public static  void  main(String[] args)
    {
        Configuration cfg=new Configuration();
        cfg.configure("OneToMany.cfg.xml");
        Transaction transaction=null;
        try(SessionFactory sessionFactory=cfg.buildSessionFactory();
        Session session=sessionFactory.openSession();
        Scanner scanner=new Scanner(System.in)) {
            Student student = new Student();
            logger.info("Enter User name :");
            String studentName = scanner.next();

            List<String> list = new ArrayList<>();
            logger.info("Enter number of course to assign :");
            int assignNumber = scanner.nextInt();
            for (int i = 0; i < assignNumber; i++) {
                logger.info("Enter the name of course");
                String courseName = scanner.next();
                list.add(courseName);
            }
            student.setCourseName(list);
            student.setName(studentName);

            transaction=session.beginTransaction();
            session.save(student);
            transaction.commit();
            logger.info("Successful");
        }
        catch (Exception e)
        {
            transaction.rollback();
            logger.error(e);
        }


    }
}
