package org.hibernate.mapping.oneToMany;

import java.util.List;

public class Student {
    int rollNo;
    String name;
    List<String> courseName;

    public Student(){}
    public Student(int rollNo,String name){
        this.rollNo=rollNo;

    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCourseName(List<String> courseName) {
        this.courseName = courseName;
    }

    public List<String> getCourseName() {
        return courseName;
    }
}
