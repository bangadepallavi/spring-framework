package org.hibernate.mapping.oneToMany;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Course {
    int id;
    String course;

    public Course(){}
    public Course(String course){
    this.course=course;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}
