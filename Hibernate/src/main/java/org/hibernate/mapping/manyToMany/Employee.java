package org.hibernate.mapping.manyToMany;

import java.util.Set;

public class Employee {
    int id;
    String name;
    Set<Allowance> allowances;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setAllowances(Set<Allowance> allowances) {
        this.allowances = allowances;
    }

    public Set<Allowance> getAllowances() {
        return allowances;
    }


}
