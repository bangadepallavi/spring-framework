package org.hibernate.mapping.manyToMany;

import java.util.Set;

public class Allowance {
    int allowanceNo;
    String allowanceName;
    Set<Employee> employee;

    public void setAllowanceNo(int allowanceNo) {
        this.allowanceNo = allowanceNo;
    }

    public int getAllowanceNo() {
        return allowanceNo;
    }


    public String getAllowanceName() {
        return allowanceName;
    }

    public void setAllowanceName(String allowanceName) {
        this.allowanceName = allowanceName;
    }

    public void setEmployee(Set<Employee> employee) {
        this.employee = employee;
    }

    public Set<Employee> getEmployee() {
        return employee;
    }
}
