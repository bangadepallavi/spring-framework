package org.hibernate.mapping.manyToMany;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashSet;
import java.util.Set;

public class EmployeeImp {
    public static final Logger logger=LogManager.getLogger(EmployeeImp.class);
    public static void main(String[] args)
    {
        Configuration configuration=new Configuration();
        configuration.configure("ManyToMany.cfg.xml");
        try(SessionFactory factory=configuration.buildSessionFactory();
            Session session=factory.openSession())
        {
        Set<Allowance> s=new HashSet<>();
       Allowance allowance=new Allowance();
       allowance.setAllowanceName("Dearness allowance");
        s.add(allowance);
       allowance.setAllowanceName("HRA");
        s.add(allowance);

        Set<Allowance> s1=new HashSet<>();
       Allowance allowance1=new Allowance();
       allowance1.setAllowanceName("travelling allowcnce");
        s1.add(allowance1);
       allowance1.setAllowanceName("Meal allowance");
        s1.add(allowance1);

        Employee employee=new Employee();
        employee.setName("rashi");
        employee.setAllowances(s);

        Employee employee1=new Employee();
        employee1.setName("ankush");
        employee1.setAllowances(s1);

        Transaction transaction=session.beginTransaction();
        session.save(employee);
        session.save(employee1);
        transaction.commit();
        }
        catch (Exception e)
        {
            logger.error(e);
        }
    }
}
