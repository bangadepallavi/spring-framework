package org.hibernate.mapping.oneToOne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.Month;

import java.util.Scanner;

public class CustomerImpl {
    public static final Logger logger=LogManager.getLogger(CustomerImpl.class);
    public static  void  main(String[] args) {
        Configuration cfg = new Configuration();
        cfg.configure("OneToOne.cfg.xml");
        Transaction transaction=null;
        try(SessionFactory sessionFactory = cfg.buildSessionFactory();
            Session session = sessionFactory.openSession();
            Scanner scanner = new Scanner(System.in)) {
            LocalDateTime currentTime=LocalDateTime.now();

            Customer customer=new Customer();
            logger.info("Enter the name of customer :");
            String name=scanner.next();
            customer.setName(name);
            customer.setDateOfJoining(currentTime.toLocalDate());

            BankAccount bankAccount=new BankAccount();
            logger.info("Enter the type of account :");
            String accountType=scanner.next();
            bankAccount.setAccountType(accountType);

            customer.setBankAccount(bankAccount);
            bankAccount.setCustomer(customer);

            transaction = session.beginTransaction();
            session.save(customer);
            session.save(bankAccount);
            transaction.commit();
        }
        catch (Exception e)
        {
            transaction.rollback();
            logger.error(e);
        }


    }

}
