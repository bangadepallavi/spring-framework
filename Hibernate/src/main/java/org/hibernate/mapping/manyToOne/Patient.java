package org.hibernate.mapping.manyToOne;

import java.time.LocalDate;

public class Patient {
    int patientNo;
    String  name,disease;
    Doctor doctor;

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPatientNo() {
        return patientNo;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public void setPatientNo(int patientNo) {
        this.patientNo = patientNo;
    }

}
