package org.hibernate.mapping.manyToOne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.Month;

import java.util.Scanner;

public class DoctorImpl {
        public static final Logger logger=LogManager.getLogger(DoctorImpl.class);
        public static  void  main(String[] args) {
            Configuration cfg = new Configuration();
            cfg.configure("ManyToOne.cfg.xml");
            Transaction transaction=null;
           try( SessionFactory sessionFactory = cfg.buildSessionFactory();
            Session session = sessionFactory.openSession()) {
               Scanner scanner = new Scanner(System.in);
               Patient patient = new Patient();
               logger.info("Enter your name");
               String patientName = scanner.next();
               patient.setName(patientName);
               patient.setDisease("spondylosis");

               Patient patient1 = new Patient();
               logger.info("Enter your name");
               String name = scanner.next();
               patient1.setName(name);
               patient1.setDisease("spondylosis");

               Doctor doctor = new Doctor();
               doctor.setName("Dr.Raut");
               doctor.setSpecialization("Orthologist");

               patient.setDoctor(doctor);
               patient1.setDoctor(doctor);

               transaction = session.beginTransaction();
               session.persist(patient);
               session.persist(patient1);

               transaction.commit();
           }
           catch (Exception e)
           {
               transaction.rollback();
               logger.error(e);
           }
        }
}
