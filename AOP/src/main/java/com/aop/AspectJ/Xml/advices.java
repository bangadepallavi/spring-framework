package com.aop.AspectJ.Xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.springframework.aop.AfterReturningAdvice;
//import org.springframework.aop.MethodBeforeAdvice;
import org.aspectj.lang.annotation.*;
@Aspect
public class advices {
    public static final Logger logger=LogManager.getLogger(advices.class);
    public void beforeMethod()
    {
        logger.info("Welcome to LocationGuru");
    }

    public void afterReturningMethod()
    {
        logger.info("Thank you for using this service");
    }

}
