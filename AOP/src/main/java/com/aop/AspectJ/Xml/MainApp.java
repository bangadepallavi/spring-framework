package com.aop.AspectJ.Xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MainApp {
    public static final Logger logger=LogManager.getLogger(MainApp.class);
    public static void main(String[] args)
    {
        try(Scanner scanner=new Scanner(System.in)) {
            ApplicationContext context = new ClassPathXmlApplicationContext("AspectJConfiguration.xml");
            Employee employee = (Employee) context.getBean( "employee");
            String url = "jdbc:postgresql://localhost:5432/internship_samples";
            String user = "postgres";
            String password = "postgres";
            try (Connection connection = DriverManager.getConnection(url, user, password)) {

                    logger.info("Enter the choice in word (ex:Enter 'one' for New registration') :");
                    logger.info("1) New registration ");
                    logger.info("2) Registration verification");
                    String choice=scanner.next();
                    switch (choice) {

                        case "one":
                            employee.registration(connection);
                            break;

                        case "two":
                            logger.info("Please verify your registration: ");

                                logger.info("Enter your userId : ");
                                 String userId=scanner.next();
                                 logger.info("Enter your password :");
                                 String userPassword=scanner.next();
                                employee.verification(userId,userPassword,connection);
                                break;


                        default:
                            logger.info("Please enter the proper choice in word(ex:'one')");
                    }

                }
                logger.error("Process is terminated");

            } catch (SQLException | NullPointerException |NoSuchElementException exception) {
                logger.error(exception);
            } catch (Exception exception) {
                logger.error(exception);
            }
        }

    }

