package com.aop.implementation;
import java.lang.reflect.*;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.AfterReturningAdvice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class advices implements MethodBeforeAdvice,AfterReturningAdvice {
    public static final Logger logger=LogManager.getLogger(advices.class);
    @Override
    public void before(Method method,Object[] args,Object target)
    {
        logger.info("Welcome to LocationGuru");
    }
    @Override
    public void afterReturning(Object returnValue,Method method,Object[] args,Object target)
    {
        logger.info("Thank you for using this service");
    }

}
