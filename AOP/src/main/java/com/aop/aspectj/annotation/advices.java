package com.aop.aspectj.annotation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class advices {
    public static final Logger logger=LogManager.getLogger(advices.class);
    @Pointcut("execution(* com.aop.aspectj.annotation.*.*(..))")
    public void services(){}

    @Before("services()")
    public void beforeMethod()
    {
        logger.info("Welcome to LocationGuru");
    }

    @After("services()")
    public void afterReturningMethod()
    {
        logger.info("Thank you for using this service");
    }

}
