package com.aop.aspectj.annotation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Employee {
    private int id;
    private String firstName,lastName,userId,password;
    public static final Logger logger=LogManager.getLogger(Employee.class);
    public void registration(Connection connection)
    {
        try(Scanner scanner=new Scanner(System.in))
        {
                try(PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Employee VALUES(?,?,?,?,?) ")) {
                    logger.info("Enter  id :");
                    id=scanner.nextInt();
                    logger.info("Enter  first name:");
                    firstName=scanner.next();
                    logger.info("Enter  last name :");
                    lastName=scanner.next();
                    logger.info("Enter userId :");
                    userId=scanner.next();
                    logger.info("Enter password :");
                    password=scanner.next();
                    preparedStatement.setInt(1, id);
                    preparedStatement.setString(2, firstName);
                    preparedStatement.setString(3, lastName);
                    preparedStatement.setString(4, userId);
                    preparedStatement.setString(5,password);
                    preparedStatement.executeUpdate();
                    logger.info("Successfully inserted.");

                }
                catch (SQLException |InputMismatchException |NumberFormatException exception) {
                    logger.error("Can not insert Invalid data ");

                }

        }
        catch (NullPointerException exception)
        {
            logger.error(exception);
        }

    }


    public void verification(String userId,String passwords,Connection connection)
    {
        try(PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM employee WHERE userid='"+userId+"' AND password='"+passwords+"'"); ResultSet resultSet = preparedStatement.executeQuery())
        {
            if(resultSet.next())
            {
            logger.info("Id : {}  ",resultSet.getInt("id") );
            logger.info(" First Name : {} ",resultSet.getString("firstname"));
            logger.info("Last Name : {} ", resultSet.getString("lastname"));
            logger.info(" UserID : {} ",resultSet.getString("userid"));
            logger.info(" Password : {}",resultSet.getString("password"));
                if(userId.equals(resultSet.getString(4)) && passwords.equals(resultSet.getString(5)) )
                {
                logger.info("User Id and password is verified...");
                }
                else
                {
                logger.info("User Id and password is incorrect...");
                }
            }
            else
            {
                logger.error("Their is no employee having this user Id and password... ");
            }

        }
        catch (SQLException |NullPointerException exception)
        {
            logger.error(exception);
        }
    }
}
