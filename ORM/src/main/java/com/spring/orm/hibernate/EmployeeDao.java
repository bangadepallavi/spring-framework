package com.spring.orm.hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Propagation;

import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.*;

public class EmployeeDao {
    HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate
                                             hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }
 public void saveEmployee(Employee object) {
 hibernateTemplate.save(object);
    }

    public void deleteEmployee(Employee object) {
        hibernateTemplate.delete(object);
    }

    public void updateEmployee(Employee object) {
                 hibernateTemplate.update(object);
    }

    public List<Employee> getEmployees(){
//        List<Employee> list= new ArrayList<>();
//        list=hibernateTemplate.loadAll(Employee.class);
        return hibernateTemplate.loadAll(Employee.class);
    }

}

