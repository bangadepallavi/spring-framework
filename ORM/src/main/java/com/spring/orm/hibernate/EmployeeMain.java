package com.spring.orm.hibernate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.transaction.Transactional;


public class EmployeeMain {
    public static final Logger logger =LogManager.getLogger(EmployeeMain.class);

    public static void main(String args[])
    {
        try (Scanner scanner=new Scanner(System.in)){
            ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
            EmployeeDao employeeDao = (EmployeeDao) context.getBean("id4");
            logger.info("Enter your choice :");
            logger.info("1)Employee registration");
            logger.info("2)Update employee");
            logger.info("3)Delete employee");
            logger.info("4)Display all the employee");
            int choice=scanner.nextInt();
            Employee employee=new Employee();
            switch (choice)
            {
                case 1:logger.info("Enter the first name of employee");
                     String firstName=scanner.next();
                    employee.setFirstName(firstName);
                    logger.info("Enter the last name of employee");
                    String lastName=scanner.next();
                    employee.setSecondName(lastName);
                    logger.info("Enter the login id of employee");
                    String loginId=scanner.next();
                    employee.setLoginId(loginId);
                    logger.info("Enter the password of employee");
                    String password=scanner.next();
                    employee.setPassword(password);
                    logger.info("Enter the department name of employee");
                    String deptId=scanner.next();
                    employee.setDeptId(deptId);
                    employeeDao.saveEmployee(employee);
                    break;
                case 2:logger.info("What you want to update ?");
                    logger.info("1)Login id");
                    logger.info("2)Password");
                    int option=scanner.nextInt();
                    logger.info("Enter the id to update the record :");
                    int idupdate=scanner.nextInt();
                    employee.setId(idupdate);
                    if(option==1){
                        logger.info("Enter new login id :");
                        String newLoginId = scanner.next();
                        employee.setLoginId(newLoginId);
                    }else if (option==2){
                        logger.info("Enter new password :");
                        String newPassword=scanner.next();
                        employee.setPassword(newPassword);
                    }
                    employeeDao.updateEmployee(employee);
                    break;
                case 3:
                    logger.info("Enter the id to delete the record :");
                    int idDelete=scanner.nextInt();
                    employee.setId(idDelete);
                    employeeDao.deleteEmployee(employee);
                    break;
                case 4: List<Employee> list=employeeDao.getEmployees();
                    for (Employee e :list)
                     logger.info("{}",e.getFirstName());
                    break;
                default:logger.error("Enter the proper choice");
            }
        }
        catch (Exception e)
        {
            logger.error(e);
        }

    }
}
