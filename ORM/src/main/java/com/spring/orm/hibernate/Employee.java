package com.spring.orm.hibernate;

public class Employee {
    int id;
    String firstName,secondName,loginId,password;
    String deptId;
    public Employee(String firstName,String secondName,String loginId,String password,String deptId)
    {
        this.firstName=firstName;
        this.secondName=secondName;
        this.loginId=loginId;
        this.password=password;
        this.deptId=deptId;

    }

    public Employee() {
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getPassword() {
        return password;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

//    public String toString() {
//        return "Employee{" +
//                "id=" + id +
//                ", firstName='" + firstName + '\'' +
//                ", secondName='" + secondName + '\'' +
//                ", loginId='" + loginId + '\'' +
//                ", password='" + password + '\'' +
//                ", deptId='" + deptId + '\'' +
//                '}';
//    }
}
