package org.spring.jdbc.template;

public class StudentDetails {
    private int rollno;
    private String sname;
    private int age;
public  StudentDetails()
{

}
    public StudentDetails(int updateRoll , int updateAge) {
        this.rollno=updateRoll;
        this.age=updateAge;
    }

    public StudentDetails(int deleteRollno) {
        this.rollno=deleteRollno;
    }

    public StudentDetails(int rollNo, String studentName, int age) {
        this.rollno = rollNo;
        this.sname = studentName;
        this.age = age;
    }

    public int getRollno() {
        return rollno;
    }

    public void setRollno(int rollno) {
        this.rollno=rollno;
    }

    public void setSname(String sname)
    {
        this.sname=sname;
    }

    public String getSname() {
        return sname;
    }
    public void setAge(int age)
    {
        this.age=age;
    }

    public int getAge() {
        return age;
    }
    public String toString(){
        return rollno+" "+sname+" "+age;
    }
}
