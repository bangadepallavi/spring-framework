package org.spring.jdbc.template;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.InvalidClassException;
import java.io.WriteAbortedException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class StudentMain {
    public static final Logger logger= LogManager.getLogger(StudentMain.class);
    public static void main(String[] args)
    {       Scanner scanner = new Scanner(System.in);
            ApplicationContext applicationContext = new ClassPathXmlApplicationContext("TemplateConfiguration.xml");
            StudentImplementation studentDao = (StudentImplementation) applicationContext.getBean("edao");
            logger.info("Enter choice");
            logger.info("1) insert student details");
            logger.info("2) select student details");
            logger.info("3) update student details");
            logger.info("4) delete student details");
            int n = scanner.nextInt();
            switch (n) {
                case 1:
                    logger.info("Enter roll no of student : ");
                    int rollNo = scanner.nextInt();
                    logger.info("Enter name of student :");
                    String studentName = scanner.next();
                    logger.info("Enter age of student :");
                    int age = scanner.nextInt();
                    Boolean b = studentDao.insertStudent(new StudentDetails(rollNo, studentName, age));
                    if (!b) {
                        logger.error("Successfully inserted");
                    } else {
                        logger.info("Error : not inserted ");
                    }
                        break;
                case 2:
                        logger.info("Enter roll no");
                        int selectRollNo = scanner.nextInt();
                        List<StudentDetails> list = studentDao.selectStudent(selectRollNo);
                        for (StudentDetails student : list) {
                            logger.info("Roll no :{} , Name : {} ,Age : {} ",student.getRollno(),student.getSname(),student.getAge());
                        }
                    break;
                case 3:
                    logger.info("Enter the rollno to update data :");
                    int updateRoll = scanner.nextInt();
                    logger.info("Enter the new age to update :");
                    int updateAge = scanner.nextInt();
                    studentDao.updateStudent(new StudentDetails(updateRoll, updateAge));
                    break;
                case 4:
                        logger.info("Enter the rollno to delete data :");
                        int deleteRollNo = scanner.nextInt();
                        studentDao.deleteStudent(new StudentDetails(deleteRollNo));
                        break;
                default:
                    logger.error("Enter the proper choice :");
            }
        scanner.close();
    }

}
