package org.spring.jdbc.template;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.ResultSetExtractor;

public class StudentImplementation {
    public static final Logger logger = LogManager.getLogger(StudentImplementation.class);
    private JdbcTemplate jdbcTemplate;
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate=jdbcTemplate;
    }

    public Boolean insertStudent(StudentDetails student) {
        String query="insert into student values(?,?,?)";
        return jdbcTemplate.execute(query, new PreparedStatementCallback<Boolean>() {

            public Boolean doInPreparedStatement(PreparedStatement preparedStatement) throws SQLException {
                try {
                    preparedStatement.setInt(1, student.getRollno());
                    preparedStatement.setString(2, student.getSname());
                    preparedStatement.setInt(3, student.getAge());
                } catch (SQLException  e) {
                    logger.error(e);
                }
                return preparedStatement.execute();
            }

        });
    }

public List<StudentDetails> selectStudent(int roll)
{
    return jdbcTemplate.query("select * from student where rollno= ?",new Object[]{roll}, new ResultSetExtractor<List<StudentDetails>>()
    {
        public List<StudentDetails> extractData(ResultSet resultSet)
        { List<StudentDetails> list= new ArrayList<>();
            try{
            while (resultSet.next())
            {
                StudentDetails student=new StudentDetails();
                student.setRollno(resultSet.getInt(1));
                student.setSname(resultSet.getString(2));
                student.setAge(resultSet.getInt(3));
                list.add(student);
            }
            }
            catch (SQLException  e)
            {
                logger.error(e);
            }
            return list;
        }
    });
}

    public void updateStudent(StudentDetails student) {
        String query="update student set age= ? where rollno=?";
        int i =jdbcTemplate.update(query,student.getAge(),student.getRollno());
        if(i>=0){logger.info("updated successfully");}
        else{logger.error("updated to update");}

    }

    public void deleteStudent(StudentDetails student) {
        String query="delete  from student where rollno=?";
        int i=jdbcTemplate.update(query,student.getRollno());
        if(i>0){logger.info("deleted successfully");}
        else if (i==0){logger.error("no record found");}
        else {logger.error("unable to deleted");        }
    }
}

